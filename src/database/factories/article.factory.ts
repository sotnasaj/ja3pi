import { Article } from 'src/api/articles/article.entity';
import { define, factory } from 'typeorm-seeding';
import * as Faker from 'faker';
import { User } from 'src/api/users/user.entity';

define(Article, (faker: typeof Faker) => {
    const article = new Article({
        articleUrl: faker.internet.url(),
        user: factory(User)() as any
    })
    return article;
})