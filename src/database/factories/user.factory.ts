import { User } from 'src/api/users/user.entity';
import { define } from 'typeorm-seeding';
import * as Faker from 'faker';

define(User, (faker: typeof Faker) => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const username = faker.internet.userName(firstName,lastName);
    const user = new User({
        userName: username,
        firstName: firstName,
        lastName: lastName,
        birthday: faker.date.past(),
        email: faker.internet.email(),
        password: username
    });
    return user;
})