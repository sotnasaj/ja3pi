import { Seeder, Factory } from 'typeorm-seeding';
import { Article } from 'src/api/articles/article.entity';

export default class CreateArticles implements Seeder {
    public async run(factory: Factory): Promise<any> {
        await factory(Article)().createMany(10);
    }
}