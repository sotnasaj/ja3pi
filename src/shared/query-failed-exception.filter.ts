import { QueryFailedError } from 'typeorm';
import { ArgumentsHost, ExceptionFilter, HttpStatus, Catch } from '@nestjs/common';
import { Response } from 'express'
import { ErrorResponse } from './error-response.interface';

@Catch(QueryFailedError)
export class QueryFailedExceptionFilter implements ExceptionFilter {
    catch(exception: QueryFailedError, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const response = context.getResponse<Response>();
        const request = context.getRequest<Request>();
        const { url } = request;
        const { message } = exception;
        const errorResponse: ErrorResponse = {
            path: url,
            timestamp: new Date().toISOString(),
            message: message,
        };
        response.status(HttpStatus.CONFLICT).json(errorResponse);
    }
}