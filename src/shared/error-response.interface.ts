export interface ErrorResponse {
    timestamp: string,
    path: string,
    message: string
}