import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { join } from "path";

export default () => ({
    type: process.env.DB_TYPE || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT, 10) || 5432,
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'password',
    database: process.env.DB_DATABASE || 'news_db',
    entities: [join(__dirname, '../', 'api/**/*.entity{.ts,.js}')],
}) as TypeOrmModuleOptions;

