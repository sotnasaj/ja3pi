export default () => ({
    globalPrefix: 'api',
    port: parseInt(process.env.APP_PORT, 10) || 7000,
    jwtSecret: process.env.JWTSECRET,
    jwtExpires: parseInt(process.env.JWTEXPIRES, 10) || 3600,
})