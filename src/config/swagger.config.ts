import { DocumentBuilder } from "@nestjs/swagger";

export const swaggerOptions = new DocumentBuilder()
    .setBasePath('api')
    .addBearerAuth()
    .setTitle('JA3PI API')
    .setDescription('Just another aggregator API swagger documentation')
    .setVersion('0.2')
    .addTag('ja3pi')
    .build();