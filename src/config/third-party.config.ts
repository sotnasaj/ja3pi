export default () => ({
    nytimes: {
        key: process.env.NYTIMES_KEY,
        endpoint: 'https://api.nytimes.com/svc/search/v2/articlesearch.json',
    },
    guardians: {
        key: process.env.GUARDIANS_KEY,
        endpoint: 'https://content.guardianapis.com/search',
    },
    newsapi: {
        key: process.env.NEWSAPI_KEY,
        endpoint: 'https://newsapi.org/v2/everything',
    },
})