import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import AppConfig from './app.config';
import DatabaseConfig from './database.config';
import ThirdPartyConfig from './third-party.config';

@Module({
    imports: [NestConfigModule.forRoot({
        isGlobal: true,
        load: [
            () => ({ app: AppConfig() }),
            () => ({ database: DatabaseConfig() }),
            () => ({ thirdParty: ThirdPartyConfig() }),
        ]
    })]
})
export class ConfigModule { }