import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { map } from 'rxjs/operators';
import { AbstractNewsService } from '../abstractNewsService';
// Services
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/common';
// Interfaces & DTO's
import { NewsApiDto } from './dto/newsapi-query.dto';
import { INewsApiResponse } from './interfaces/newsapi-response.interface';
import { SearchResponse } from 'src/api/ja3pi/interfaces/searchResponse';
import { SearchDTO } from 'src/api/ja3pi/dto/search-query.dto';

@Injectable()
export class NewsApiService extends AbstractNewsService {
    private readonly _endpoint: string;
    private readonly _apiKey: string;

    constructor(
        private readonly _configService: ConfigService,
        private readonly _httpService: HttpService,
    ) {
        super();
        this._endpoint = _configService.get('thirdParty.newsapi.endpoint');
        this._apiKey = _configService.get('thirdParty.newsapi.key');
    }

    protected searchNews(
        query: SearchDTO,
    ): Observable<AxiosResponse<INewsApiResponse>> {
        const queryData = new NewsApiDto({
            q: query.q,
            from: query.from_date,
            to: query.to_date,
            page: query.page,
        });
        return this._httpService.get<INewsApiResponse>(
            this._endpoint + queryData.toUri() + `&apiKey=${this._apiKey}`,
        );
    }

    protected mapResponse(response: Observable<AxiosResponse<INewsApiResponse>>): Observable<SearchResponse[]> {
        return response.pipe(
            map(response => {
                if (!response.data.articles) return []
                else return response.data.articles
            }),
            map(articles =>
                articles.map<SearchResponse>(item => {
                    return {
                        source: 'News Api',
                        tittle: item.tittle,
                        publicationDate: item.publishedAt,
                        section: 'not provided',
                        type: 'not provided',
                        ulr: item.url
                    }
                }))
        );
    }
}