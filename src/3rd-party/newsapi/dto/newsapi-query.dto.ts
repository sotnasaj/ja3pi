export class NewsApiDto {

    public q: string;
    public from: string | undefined;
    public sortBy: string | undefined;
    public to: string | undefined;
    public page: number | undefined;

    constructor(partial: Partial<NewsApiDto>) {
        Object.assign(this, partial);
    }

    toUri() {
        const uri: string[] = [];
        uri.push(`?q=${this.q}`);
        if (this.from) uri.push(`&being_date=${this.from.replace(/-/g, '')}`);
        if (this.to) uri.push(`&end_date=${this.to.replace(/-/g, '')}`);
        uri.push(`&pageSize=${10}`);
        if (this.page) uri.push(`&page=${this.page}`);
        return uri.join('');
    }
}