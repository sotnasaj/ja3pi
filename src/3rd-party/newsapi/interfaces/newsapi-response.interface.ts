export interface INewsApiResponse {
    status: string,
    totalResults: number,
    articles: Articles[]
}

interface Articles {
    source: {
        id: string,
        name: string
    },
    author: string,
    tittle: string,
    description: string,
    url: string,
    urlToImage: string,
    publishedAt: string,
    content: string,
}