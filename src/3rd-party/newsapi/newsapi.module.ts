import { Module, HttpModule } from '@nestjs/common';
import { NewsApiService } from './newsapi.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule, ConfigModule],
  providers: [NewsApiService],
  exports: [NewsApiService]
})
export class NewsApiModule { }
