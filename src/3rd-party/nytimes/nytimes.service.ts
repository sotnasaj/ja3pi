import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
//Services
import { ConfigService } from '@nestjs/config';
//Interfaces and DTO's
import { NYTimesQuery } from './dto/nytimes-query.dto';
import { NYTimesResponse } from './interfaces/nytimesResponse';
import { AbstractNewsService } from '../abstractNewsService';
import { SearchDTO } from 'src/api/ja3pi/dto/search-query.dto';
import { SearchResponse } from 'src/api/ja3pi/interfaces/searchResponse';
import { map } from 'rxjs/operators';

@Injectable()
export class NYTimesService extends AbstractNewsService {
  private readonly _endpoint: string;
  private readonly _apiKey: string;

  constructor(
    private readonly _configService: ConfigService,
    private readonly _httpService: HttpService,
  ) {
    super();
    this._endpoint = _configService.get('thirdParty.nytimes.endpoint');
    this._apiKey = _configService.get('thirdParty.nytimes.key');
  }

  protected searchNews(query: SearchDTO): Observable<AxiosResponse<NYTimesResponse>> {
    const queryData = new NYTimesQuery({
      q: query.q,
      beingDate: query.from_date,
      endDate: query.to_date,
      page: query.page,
    });
    return this._httpService.get<NYTimesResponse>(
      this._endpoint + queryData.toUri() + `&api-key=${this._apiKey}`,
    );
  }
  
  protected mapResponse(
    response: Observable<AxiosResponse<NYTimesResponse>>,
  ): Observable<SearchResponse[]> {
    return response.pipe(
      map(response => response.data.response.docs),
      map(docs =>
        docs.map<SearchResponse>(item => {
          return {
            source: 'The New York Times',
            tittle: item.abstract,
            publicationDate: item.pub_date,
            section: item.section_name,
            type: item.document_type,
            ulr: item.web_url,
          };
        }),
      ),
    );
  }
}
