//Modules
import { Module, HttpModule } from '@nestjs/common';
//Services
import { NYTimesService } from './nytimes.service';

@Module({
    imports: [HttpModule],
    providers: [NYTimesService],
    exports: [NYTimesService]
    
})
export class NYTimesModule {}
