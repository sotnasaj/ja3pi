import { Observable } from "rxjs";
import { AxiosResponse } from 'axios';
import { SearchDTO } from "src/api/ja3pi/dto/search-query.dto";
import { SearchResponse } from "src/api/ja3pi/interfaces/searchResponse";

export abstract class AbstractNewsService {

    public search(query: SearchDTO): Observable<SearchResponse[]>{
        const result = this.searchNews(query);
        this.logSearchRequest('Api');
        return this.mapResponse(result);
    }

    protected logSearchRequest(source: string): void {
        console.log(`Api request to ${source}`);
    }

    protected abstract searchNews(query: SearchDTO): Observable<AxiosResponse>;
    protected abstract mapResponse(response: Observable<AxiosResponse>): Observable<SearchResponse[]>;
}