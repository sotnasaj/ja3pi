export class GuardiansQuery {

    public q: string;
    public beingDate: string | undefined;
    public endDate: string | undefined;
    public page: number | undefined;

    constructor(partial: Partial<GuardiansQuery>) {
        Object.assign(this, partial);
    }

    toUri() {
        const uri: string[] = [];
        uri.push(`?q=${this.q}`);
        if (this.beingDate) uri.push(`&from-date=${this.beingDate}`);
        if (this.endDate) uri.push(`&to-date=${this.endDate}`);
        if (this.page) uri.push(`&page=${this.page}`)
        return uri.join('');
    }
}