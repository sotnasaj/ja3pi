import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
//Services
import { ConfigService } from '@nestjs/config';
//Interfaces and DTO's
import { GuardiansQuery } from './dto/guardians-query.dto';
import { GuardiansResponse } from './interfaces/guardiansResponse';
import { AbstractNewsService } from '../abstractNewsService';
import { SearchResponse } from 'src/api/ja3pi/interfaces/searchResponse';
import { SearchDTO } from 'src/api/ja3pi/dto/search-query.dto';

@Injectable()
export class GuardiansService extends AbstractNewsService {
  private readonly _endpoint: string;
  private readonly _apiKey: string;

  constructor(
    private readonly _configService: ConfigService,
    private readonly _httpService: HttpService,
  ) {
    super();
    this._endpoint = _configService.get('thirdParty.guardians.endpoint');
    this._apiKey = _configService.get('thirdParty.guardians.key');
  }

  protected searchNews(
    query: SearchDTO,
  ): Observable<AxiosResponse<GuardiansResponse>> {
    const queryData = new GuardiansQuery({
      q: query.q,
      beingDate: query.from_date,
      endDate: query.to_date,
      page: query.page,
    });
    return this._httpService.get<GuardiansResponse>(
      this._endpoint + queryData.toUri() + `&api-key=${this._apiKey}`,
    );
  }
  protected mapResponse(
    response: Observable<AxiosResponse<GuardiansResponse>>,
  ): Observable<SearchResponse[]> {
    return response.pipe(
      map(response => response.data.response.results),
      map(results =>
        results.map<SearchResponse>(item => {
          return {
            source: 'The Guardian',
            tittle: item.webTitle,
            publicationDate: item.webPublicationDate,
            section: item.sectionName,
            type: item.type,
            ulr: item.webUrl,
          };
        }),
      ),
    );
  }
}