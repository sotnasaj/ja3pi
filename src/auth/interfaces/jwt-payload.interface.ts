export interface IJwtPayload {
    userId: number,
    userName: string,
    email: string
}