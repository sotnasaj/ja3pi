
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthController } from './auth.controller';
import { ConfigService } from '@nestjs/config';
import { UsersModule } from 'src/api/users/users.module';

@Module({
    imports: [
        UsersModule,
        PassportModule.register({
            defaultStrategy: 'jwt', property: 'user',
            session: false,
        }),
        JwtModule.registerAsync({
            inject: [ConfigService],
            useFactory: async (config: ConfigService): Promise<JwtModuleOptions> => ({
                secret: config.get('app.jwtSecret'),
                signOptions: { expiresIn: config.get('app.jwtExpires') },
            }),
        })],
    providers: [AuthService, JwtStrategy],
    exports: [PassportModule, JwtModule],
    controllers: [AuthController],
})
export class AuthModule { }