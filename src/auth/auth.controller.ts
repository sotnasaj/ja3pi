import { Controller, Post, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import LoginUserDto from 'src/api/users/dto/login-user.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth endpoint')
@Controller('auth')
export class AuthController {

    constructor(private readonly _authService: AuthService) { }

    @Post('/login')
    @UsePipes(new ValidationPipe({ transform: true }))
    async login(@Body() user: LoginUserDto) {
        const token = await this._authService.login(user);
        return token;
    }
}
