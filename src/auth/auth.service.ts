import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/api/users/users.service';
import LoginUserDto from 'src/api/users/dto/login-user.dto';
import { compare } from 'bcryptjs';
import { IJwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {
    constructor(
        private readonly _jwtService: JwtService,
        private readonly _userService: UsersService,
    ) { }

    async login(userCredentials: LoginUserDto): Promise<{ accessToken: string }> {
        const user = await this._userService.findOne({
            userName: userCredentials.userName
        });
        if (!user) throw new UnauthorizedException('Invalid username/password');
        const verifiedPassword = await compare(userCredentials.password, user.password);
        if (!verifiedPassword) throw new UnauthorizedException('Invalid username/password');
        const payload: IJwtPayload = {
            userId: user.id,
            email: user.email,
            userName: user.userName
        }
        return { accessToken: this._jwtService.sign(payload) }
    };
}