import { Injectable } from '@nestjs/common';
import { Observable, zip } from 'rxjs';
//Interfaces and DTO
import { SearchDTO } from './dto/search-query.dto';
import { SearchResponse } from './interfaces/searchResponse';
//Services
import { AbstractNewsService } from 'src/3rd-party/abstractNewsService';

@Injectable()
export class Ja3piService {
    search(query: SearchDTO, newsService?: AbstractNewsService ): Observable<SearchResponse[]> {
        return newsService.search(query);
    }
}
