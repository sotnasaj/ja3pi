import { Module, HttpModule } from '@nestjs/common';
//Services
import { Ja3piService } from './ja3pi.service';
//Modules
import { NYTimesModule } from '../../3rd-party/nytimes/nytimes.module';
import { GuardiansModule } from '../../3rd-party/guardians/guardians.module';
import { AuthModule } from '../../auth/auth.module';
import { NewsApiModule } from 'src/3rd-party/newsapi/newsapi.module';
//Controllers
import { Ja3piController } from './ja3pi.controller';

@Module({
    imports: [AuthModule, HttpModule, NYTimesModule, GuardiansModule, NewsApiModule],
    controllers: [Ja3piController],
    providers: [Ja3piService]
})
export class Ja3piModule { }
