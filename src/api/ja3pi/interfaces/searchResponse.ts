export interface SearchResponse{
    ulr: string,
    tittle: string,
    section: string,
    publicationDate: string,
    type: string,
    source?: string 
}