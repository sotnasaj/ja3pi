import {
    Controller, Get, Query, UsePipes,
    ValidationPipe, UseGuards
} from '@nestjs/common';
//Services
import { Ja3piService } from './ja3pi.service';
// interfaces and DTO's
import { SearchDTO } from './dto/search-query.dto';
//Guards
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
//Modules
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { NewsApiService } from 'src/3rd-party/newsapi/newsapi.service';
import { GuardiansService } from 'src/3rd-party/guardians/guardians.service';
import { NYTimesService } from 'src/3rd-party/nytimes/nytimes.service';
import { map} from 'rxjs/operators';
import { zip } from 'rxjs';

@ApiTags('JA3PI search endpoint')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('ja3pi')
export class Ja3piController {
    constructor(private readonly _ja3piService: Ja3piService,
        private readonly _nytimesService: NYTimesService,
        private readonly _guardiansService: GuardiansService,
        private readonly _newsApiService: NewsApiService) { }

    @Get('search')
    @UsePipes(new ValidationPipe({ transform: true }))
    searchNYTimes(@Query() searchQuery: SearchDTO) {

        if (searchQuery.source === 'nytimes')
            return this._ja3piService.search(searchQuery,this._nytimesService);

        if (searchQuery.source === 'guardians')
            return this._ja3piService.search(searchQuery,this._guardiansService);

        if (searchQuery.source === 'newsapi')
            return this._ja3piService.search(searchQuery, this._newsApiService);

        return zip(
            this._ja3piService.search(searchQuery,this._nytimesService),
            this._ja3piService.search(searchQuery,this._guardiansService),
            this._ja3piService.search(searchQuery, this._newsApiService)
        ).pipe(
            map(response => response[0].concat(response[1],response[2])),

        );

    }
}
