import {
    Controller, Post, Body, ValidationPipe, UsePipes,
    Get, Param, ParseIntPipe, HttpCode, Delete, UseGuards,
    Req, ForbiddenException
} from '@nestjs/common';
import { ArticlesService } from './articles.service';
import CreateArticleDto from './dto/create-article.dto';
import { from } from 'rxjs';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Request } from 'express'
import { IJwtPayload } from 'src/auth/interfaces/jwt-payload.interface';

@ApiTags('articles')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('users/:idUser/articles')
@UsePipes(new ValidationPipe({ transform: true }))
export class ArticlesController {
    constructor(

        private readonly _articleService: ArticlesService,
    ) { }

    @Post()
    create(@Param('idUser', ParseIntPipe) idUser,
        @Body() articleData: CreateArticleDto,
        @Req() request: Request
    ) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._articleService.save(idUser, articleData));
    }

    @HttpCode(204)
    @Delete(':idArticle')
    delete(
        @Param('idUser', ParseIntPipe) idUser,
        @Param('idArticle', ParseIntPipe) idArticle,
        @Req() request: Request) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._articleService.deleteById(idArticle));
    }

    @Get()
    findUserArticles(@Param('idUser', ParseIntPipe) idUser, @Req() request: Request) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._articleService.findUserArticles(idUser));
    }

    @Get(':idArticle')
    findUserArticlesById(
        @Param('idUser', ParseIntPipe) idUser,
        @Param('idArticle', ParseIntPipe) idArticle,
        @Req() request: Request) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._articleService.findUserArticleById(idUser, idArticle));
    }
}