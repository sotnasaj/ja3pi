import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Article } from './article.entity';
import { Repository } from 'typeorm';
import CreateArticleDto from './dto/create-article.dto';
import { User } from '../users/user.entity';

@Injectable()
export class ArticlesService {
    constructor(
        @InjectRepository(Article)
        private _articleRepository: Repository<Article>,
        @InjectRepository(User)
        private _userRepository: Repository<User>,
    ) { }

    async save(userId: number, articleData: CreateArticleDto) {
        const user = await this._userRepository.findOne({ where: { id: userId, isActive: true } });
        if (!user) return null;
        const article = new Article(articleData);
        article.user = user;
        await this._articleRepository.save(article);
        return articleData;
    }

    deleteById(id: number){
        return this._articleRepository.delete(id);
    }
    
    findUserArticles(userId: number) {
        return this._articleRepository.find({ where: { user: userId } })
    }

    findUserArticleById(userId: number, articleId: number) {
        return this._articleRepository.findOne({ where: { user: userId, id: articleId } })
    }
}