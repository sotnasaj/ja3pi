import { IsNotEmpty } from 'class-validator';

export default class CreateArticleDto{
    @IsNotEmpty()
    articleUrl: string;
}