import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
export class Article {
    @PrimaryGeneratedColumn({ name: 'id' })
    id: number;

    @Column({ name: 'article_url', nullable: false })
    articleUrl: string;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne((type) => User, (user) => user.articles )
    @JoinColumn({name: 'user_id'})
    user: User;

    constructor(partial: Partial<Article>) {
        Object.assign(this, partial);
    }
}