import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { ArticlesModule } from './articles/articles.module';
import { Ja3piModule } from './ja3pi/ja3pi.module';
import { DatabaseModule } from 'src/database/database.module';

@Module({
    imports:[UsersModule,ArticlesModule,Ja3piModule, DatabaseModule]
})
export class ApiModule {}
