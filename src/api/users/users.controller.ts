import {
    Controller, UseGuards, Post, Body, ValidationPipe,
    UsePipes, Get, ClassSerializerInterceptor, UseInterceptors,
    UseFilters, Delete, Param, ParseIntPipe, Patch, NotFoundException, HttpCode, Req, ForbiddenException,
} from '@nestjs/common';
import { map } from 'rxjs/operators';
import { from, Observable } from 'rxjs';
import { Request } from 'express'
// Guards
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
//Entities , Interfaces & DTO's
import CreateUserDto from './dto/create-user.dto';
import { User } from './user.entity';
import UpdateUserDto from './dto/update-user.dto';
// Services
import { UsersService } from './users.service';
//Filters
import { QueryFailedExceptionFilter } from '../../shared/query-failed-exception.filter';
//Modules
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { IJwtPayload } from 'src/auth/interfaces/jwt-payload.interface';
import ChangePasswordDto from './dto/change-password.dto';

@ApiTags('Users')
@UseInterceptors(ClassSerializerInterceptor)
@UseFilters(QueryFailedExceptionFilter)
@Controller('users')
export class UsersController {
    constructor(private readonly _userService: UsersService) { }

    @Post()
    @UsePipes(new ValidationPipe({ transform: true }))
    create(@Body() user: CreateUserDto) {
        return from(this._userService.register(user));
    }

    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Get(':idUser')
    findById(@Param('idUser', ParseIntPipe) idUser: number,
        @Req() request: Request): Observable<User> {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._userService.findById(idUser)).pipe(
            map(user => {
                if (!user) throw new NotFoundException('User not found');
                return user;
            }),
        );
    }

    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Get()
    findAll(): Observable<User[]> {
        return from(this._userService.findAll());
    }

    @ApiBearerAuth()
    @HttpCode(204)
    @UseGuards(JwtAuthGuard)
    @Delete(':idUser')
    delete(@Param('idUser', ParseIntPipe) idUser: number,
        @Req() request: Request) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._userService.deleteById(idUser));
    }

    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Patch(':idUser')
    update(@Param('idUser', ParseIntPipe) idUser: number,
        @Body() item: UpdateUserDto,
        @Req() request: Request) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._userService.update(idUser, item)).pipe(
            map(user => {
                if (!user) throw new NotFoundException('User not found');
                return user;
            })
        );
    }

    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Patch(':idUser/reset-password')
    changePassword(@Body() userCredentials: ChangePasswordDto,
        @Param('idUser', ParseIntPipe) idUser,
        @Req() request: Request) {
        const user = request.user as IJwtPayload;
        if (idUser !== user.userId) throw new ForbiddenException('Permission denied');
        return from(this._userService.changePassword(userCredentials));
    }
}
