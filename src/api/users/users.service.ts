import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { hashSync, hash, compare, genSalt } from 'bcryptjs';
import { Repository, DeleteResult } from 'typeorm';
// Entities, Interfaces & DTO's
import { User } from './user.entity';
import CreateUserDto from './dto/create-user.dto';
import ChangePasswordDto from './dto/change-password.dto';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ) { }

    async register(newUser: CreateUserDto): Promise<User> {
        const user = new User(newUser);
        return this.usersRepository.save(user);
    }

    async findAll(): Promise<User[]> {
        return this.usersRepository.find();
    }

    async findOne(filter: {}): Promise<User> {
        return this.usersRepository.findOne(filter);
    }

    async findById(id: number): Promise<User> {
        return this.usersRepository.findOne(id);
    }

    async update(id: number, item: Partial<User>): Promise<User> {
        await this.usersRepository.update(id, item);
        return this.usersRepository.findOne(id);
    }

    async deleteById(id: number): Promise<DeleteResult> {
        return this.usersRepository.delete(id);
    }

    async changePassword(userCredentials: ChangePasswordDto) {
        const { newPassword, oldPassword } = userCredentials;
        const user = await this.findOne({
            userName: userCredentials.userName
        });
        if (!user) throw new UnauthorizedException('Invalid username/password1');
        const verifiedPassword = await compare(oldPassword, user.password);
        if (!verifiedPassword) throw new UnauthorizedException('Invalid username/password');
        user.password = await this.hashPassword(user.password)
        await this.usersRepository.update(user.id, { password: newPassword });
        return this.usersRepository.findOne(user.id);
    }

    private async hashPassword(password: string): Promise<string> {
        const salt = await genSalt();
        const hashedPassword = await hash(password, salt);
        return hashedPassword;
    }
}
