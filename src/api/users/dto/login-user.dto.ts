import { IsNotEmpty } from 'class-validator';

export default class LoginUserDto {

    @IsNotEmpty()
    userName: string;

    @IsNotEmpty()
    password: string;

}