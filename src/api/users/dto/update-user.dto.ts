import { IsNotEmpty, IsEmail, IsOptional } from 'class-validator';

export default class UpdateUserDto {

    @IsOptional()
    @IsNotEmpty()
    firstName: string;

    @IsOptional()
    @IsNotEmpty()
    lastName: string;

    @IsEmail()
    email: string;
}