import { IsNotEmpty, IsEmail} from 'class-validator';
import { Transform } from 'class-transformer';

export default class CreateUserDto {

    @IsNotEmpty()
    firstName: string;

    @IsNotEmpty()
    lastName: string;

    @IsNotEmpty()
    userName: string;

    @IsEmail()
    email: string;

    @Transform(value => new Date(value))
    birthday: Date;

    @IsNotEmpty()
    password: string;

}