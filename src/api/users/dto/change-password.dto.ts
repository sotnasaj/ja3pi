import { IsNotEmpty } from 'class-validator';

export default class ChangePasswordDto {

    @IsNotEmpty()
    userName: string;

    @IsNotEmpty()
    oldPassword: string;

    @IsNotEmpty()
    newPassword: string;

}