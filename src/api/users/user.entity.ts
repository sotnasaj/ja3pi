import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, OneToMany} from 'typeorm';
import { Exclude } from 'class-transformer';
import { genSalt, hash } from 'bcryptjs'
import { Article } from '../articles/article.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  @Column({ name: 'first_name', nullable: false })
  firstName: string;

  @Column({ name: 'last_name', nullable: false })
  lastName: string;

  @Column({ name: 'user_name', nullable: false, unique: true })
  userName: string;

  @Column({ name: 'email', nullable: false })
  email: string;

  @Column({ name: 'birth_day', type: Date, nullable: false })
  birthday: Date;

  @Column({ name: 'is_active', default: true })
  isActive: boolean;

  @Exclude()
  @Column({ nullable: false })
  password: string;

  @BeforeInsert()
  async setPassword(password: string) {
    const salt = await genSalt();
    this.password = await hash(password || this.password, salt);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @OneToMany((type) => Article, (article) => article.user, {onDelete: 'CASCADE'}) articles: Article[]

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}