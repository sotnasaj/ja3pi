import { Module } from '@nestjs/common';
//Modules
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { ApiModule } from './api/api.module';
//Controllers
import { AppController } from './app.controller';
//Services
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
//Others

@Module({
  imports: [ConfigModule, DatabaseModule, AuthModule, ApiModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
