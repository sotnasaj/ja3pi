import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule } from '@nestjs/swagger';
import { swaggerOptions } from './config/swagger.config';
import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  app.use(helmet());
  app.setGlobalPrefix(config.get('app.globalPrefix'));
  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api/ja3pi/docs', app, document);
  await app.listen(config.get('app.port'));
}
bootstrap();
