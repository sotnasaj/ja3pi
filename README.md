<p align="center">
 <a href="https://gitlab.com/sotnasaj/ja3pi/" target="blank"><img src="https://i.pinimg.com/originals/ed/4c/91/ed4c91f9eaec7c26b9b3f747a27eacba.png" width="175" alt="JA3PI logo" /></a>
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="420" alt="Nest Logo" /></a>
</p>

***OUTDATED LOGO***
## Description

Single endpoint news aggregator for [NYTimes API](https://developer.nytimes.com/),
[The Guardian API](https://open-platform.theguardian.com/documentation/) & [News Api](https://newsapi.org/)
, made with love, effort and [NestJS](https://github.com/nestjs/nest)

## Running the app

```bash
# development
$ npm start

# watch mode
$ npm run start:dev

```
## Usage ( <3 SWAGGER!!!! )

Locally visit:

http://localhost:7000/api/ja3pi/docs

Create user [Post] does not require any authentication, so you can create your own user ! 
By now everyone can search for all users , but in a future is goind to be by roles.
For example purposes users username and password are related (almost always)  

username1 - password1
username2 - password2
username20 - password20

but you can do whatever you want.

# Here! The answers you were looking for:

## :large_orange_diamond: Which patterns does Nest.JS use? (gotta catch 'em all :red_circle:)

### :small_blue_diamond:  Decorators

As a simple definition of decorator is that they are wrappers that add behaviors. In NestJS decorators like @Injectable tell us that a dependency is automatically injected and not managed by the class that need it. Decorators associate classes with the required metadata, NestJS give us built-in decorators for classes like ```@Injectable``` as we said before, ```@Controller``` that is use to build a routing map. It also provides method decorators like ```[@Param(), @Query()]``` that let us "extract" values,  decorator for attributes @IsNotEmpty to do validations, etc. And of course we can also create our custom decorators. It is used by most of the libraries, e.g TypeOrm, class-validator, class-transformer, Joi,  Swagger , ...(there are more of course).

### :small_blue_diamond: Why?

Decorator is an extremely useful tool, it allows us "convert" a class  just by adding behaviors through a simple decorator and ***without making it a new subclass***. We can create Entities, Injectables, Validations, Extracted values, and much more. And it helps us with Single Responsibility Principle

### :small_red_triangle_down: Dependency injection

We can define DI as a way to pass dependencies, where one "object" supplies the dependencies of another object. By the use of Typescript (Decorators enable) NestJS let us produce "injectable" object with @Injectable. Providers, guards, pipes and interceptors are required to be wrapped. 

### :small_red_triangle_down: why?

The main reasons around using DI is that it can help us  implementing decouple and maintainable solutions. With DI testing can be easier with the use of mock objects. DI also eliminates static dependencies and makes them dynamic, being more configurable and they managed by an external, in this case by NestJS.

### :small_blue_diamond: Factory method

Being a creational patterns, it allows us to create object from a super class, but we can alter the result by a subclass. Nestjs has some (i think a lot) of this implementation, we can get one just by reading the way we can create an "app", where we can pass several options:
```
  const app = await NestFactory.create(AppModule, OPTIONS);
```
In  nestjs providers can also be created dynamically with factories, and the provider will be supplied by the result retrieved from the factory function.  We can also inject other providers to be use in a factory, so it gives us even more tools for our final creation.

### :small_blue_diamond: Why? 

This factories let us create dynamically,  so we can use some checks or configurations to alter the resulting object. By injecting inside factories we can even depend on other services, which is great because we wait for it to be created, a common example is a configService.

### :small_red_triangle_down:  Singleton
In few simple words, singletons can be see it as a global unique accessible "object" so the state of that object affects everyone, because there is one instance all the time. By the nature of JS (single threaded) Nestjs use singletons approach as a default for managing things like modules, controllers, providers, and others, but in some cases it also give us the flexibility to change this "scope" to things like "Request scope" where instances are created for each request. This singleton pattern are commonly use with DI in nestJS.

### :small_red_triangle_down:  Why? 
One reason or motivation is that , as i said before, the nature of JS give us the opportunity to use them in a safe way. I think the main benefit using singletons is the performance because we don't need to create instances of classes in every request. Another thing may be that the "handler chain" of the request has to access common data or context, and a singleton instance make that easier.

### :small_blue_diamond: Chain of responsibility
In a fast way, we can say that this pattern is about collaborations of a chain of handlers, where each handler or node in the chain can interact/pass/end the request. The use of this patterns derives from express and the use of middlewares along the process of a request, so nestjs that is built on top of express got them, but they create some "specialist" middlewares giving us a more structured solution, for example we got a specific middleware for Auth operations like guards, and they are executed in the right moment/place.

### :small_blue_diamond: Why?
For handling request it is common to have sequential steps, for example i don't wanna handle a request if the client doesn't have the permission or doesn't provide the needed data, so first i need to check after handling the actual request. So, with that we got a chain of steps , and its useful to avoid unnecessary work by ending the request when needed, and also it gives us a better structure solution and with better separation of responsibilities.

### :small_red_triangle_down: Facade

Facade can be summarized in a use of simplified interfaces. So, when we talk about frameworks i think is natural to think on facade patterns, because one of the benefits, there are a lot , is that low-level functionalities are commonly taken care of by the framework itself, and we can managed them by more simple interfaces. So in this case the use of facades are related to express,fastify, nestjs and also libraries. They implement facades in things like app.listen() that help us to run the server with that simple line , of course we do some steps before, but it still simpler.

### :small_red_triangle_down: Why?
I think facade tends to be related with most of the patterns and solutions, because we always want to abstract complexity, it help us avoiding interaction with huge amount of code and making it more reusable and less coupled. Using facade can also help striping things that we don't want to directly expose.

### :small_blue_diamond: Observer pattern

We can see this pattern as a mechanism that give us the feature to notify observer about events when they happen. This pattern high related with js, Node, express and not only with Nestjs. I think the most common example of this implementation is the app.on() , app.once() that let us subscribe to events like on 'error' and let us execute what we think is necessary.

### :small_blue_diamond: Why?

The main reason behind this pattern is that the observer don't have to guess or do checks to see if something is ready, instead of the unnecessary check they get notified. So for example we don't need to check every second if we receive a request or i we got an error, we will get notified.

### There are more patterns like Strategy and proxy...


## :large_orange_diamond: Which patterns can be used on your application? How those patterns could be implemented?

### :page_facing_up: Template
Template pattern can be implemented for 3rd-party services (Nytimes, guardians, newsapi), givin them a base class that tells to subclasses the necessary steps (methods)they shloud implement and the ones that are implemented in common by the base class.
For that we need to create an Abstract class with the template method (in charge to execute the steps) and divide the process in diferent methods that can be abstract (child needs to implement them) and other "already implemente".
Every subclass have to extende this previous class and fulfill the contract with the base class.

###  :cyclone: Iterator
This one can be implement in a "easier way" by using RxJS , because returning Observable we can explore (iterate) them in several ways.

###  :construction_worker: Builder
At the beginning we had 2 API, now we have three but in a future we could have 10 and the main reason of using the builder pattern is that every news API is just annexed (concatenated) in to one result so they are like pieces.
I think it looks a little bit weird in our case, because every part is kind of the same a response from a external API.

To implement this pattern first we need a interfaces with the possible parts of out final results, in this case external API responses.
Then we need concrete builder, that are responsible to implement (or execute) the needed code to actually get the parts.
In this case i think a Director is highly recommended because we can define final products like: 
'Articles search' to use API's that search in article
'Famous Search ' to use the most popular API's
'Search all' to get result from every API's


## :large_orange_diamond: A good one: what is an antipattern ?  :gem:

From the name itself , "anti" referring to an opposing force and "patter" from , well patterns , we can understand that they are things, actions, codes, attitudes or whatever thing that goes against design patterns or even against the management of a project. Anti patterns are like well know problems that we often do and provoke a lot of bad consequences, like stock code, coupled system or stock teams. Just as a comment, i think anti-patterns are more important to apply/know that the patterns, because fighting anti-patterns leave us the way "free" to implement the different design patterns on needed and avoiding a lot of (wasted time) refactoring.

Anti-patterns can live in different levels, at the beginning we can find them on a development level, where they "block" us to implement one o several patterns that we really need o we will need in a future. In this level anti-patterns tends to be related to code and modules of an applications. Here the solution is high related to mining and code refactor.

Then we can find them in a "System" level, and they are called architecture anti-patterns. This level is more abstract, we don't necessarily refers to code problem (but the code it affected at the end). Here we got a full view of a system and all the modules, here the anti-pattern solutions wants to avoid conflict on different part of the systems that inhibit change or obstacles the integration of our application with the thing we already got or the new ones that we may have in a future. At this level, a common solution is the use of layers to avoid dependencies, and a more specific example it can refers (not always) to the use of frameworks.

Finally we can talk of a "Management" level, that is even less related to code. At this level we want to fight anti-patterns to avoid rocks in the way or to improve the "fluency" of the development on a team. Patterns here are related to stuff like over planning or even conflicts with big teams. One common solution that i can find here is the use of things like Agile, because it gives us an iterative approach avoiding over planning and giving a lot of knowledge to all the team members (also stakeholders) as a consequence of the continuous feedback, and a others benefits that i don't even know at this moment.


## :large_orange_diamond:  How to implement the Dependency Injection pattern in Typescript  :warning:

We need to have some basic knolwege about reflections and decorators.

[Wikipedia](https://en.wikipedia.org/wiki/Reflection_(computer_programming))     ***Reflection*** is the ability of a process to examine, introspect, and modify its own structure and behavior
and because we are using TS we use reflect-metadata (much better). Reflection is basically code that is able to inspect other code in the same system.

[TypescriptLang](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)     A Class Decorator is declared just before a class declaration. The class decorator is applied to the constructor of the class and can be used to observe, modify, or replace a class definition. A class decorator cannot be used in a declaration file, or in any other ambient context (such as on a declare class).
The expression for the class decorator will be called as a function at runtime, with the constructor of the decorated class as its only argument.


```typescript
// we ask for a constructable function, so a class with any number/type of arguments
interface ConstructableDude<T = any> {
  new (...args: any[]): T;
}

//This is a decorator function
// But we are just returning it as the original
export function InjectableDude(dude: ConstructableDude): ConstructableDude {
  return dude;
}

export class DudeInjector {
  private injMap = new Map();

  getInstance<T>(dude: ConstructableDude): T {
    const dudeInstance = this.dudeConstructor(dude);
    return dudeInstance;
  }

  private dudeConstructor(dude: ConstructableDude) {
  // If we already got a instnace, we return this old instance.
    let currentInstance = this.injMap.get(dude);
    if (currentInstance) return currentInstance;

    // Here we are examinating with reflection
    const metaData: ConstructableDude[] = Reflect.getMetadata(
      'design:paramtypes',
      dude,
    );
    // Grab arguments
    const dudeArguments = metaData.map(args => this.dudeConstructor(args));
    // We create the new instance
    currentInstance = new dude(...dudeArguments);
    return currentInstance;
  }
}

//Here we receive a class as a "parameter" a we make it an injectabledude
@InjectableDude
export class ServiceClass {
  constructor() {}
  born() {
    return 'Hello fworld';
  }
}
```

## :beginner: Removed antipatterns

#### :unlock: Spaghetti
I think this is the most common for everyone (beginner,no-time,bad-knowledge)
First,   it is hard (painful) to understand, and also hard to maintain and extend.
The main problem is that code is hard or impossible to reuse, and it tends to be against OOP benefits and a lot of patterns to.

#### :unlock: Lava flow
This one is "less problematic" because things can still working...but...
The problem here is the question: Is this necessary? I'm not sure...
Makes us feel insecure about what are we doing, and its also a waste of unnecessary  lines!

#### :unlock: The blob
Ok, the blob in my case (not always) is the beginning of something when I'm learning something new.
The problem here is that we have a big monster for everything, even no related stuff. So it takes of the ability to reuse the code, sometimes processes or even simple data.

#### :unlock: Boat anchor
It is a bad deal! Sometimes we can add external dependencies just because it was recommended
 by the framework,community or by ourselves.
It is  not what we need or maybe we need something smaller. So with poor research it can be a waste of time, and it could also be a waste of resources because we use a huge library just to use one feature.
(And sometime we can forget to removed unused libraries. ) :shame:

#### :unlock: Fear of success
Its basically , "Its working nice, I'm afraid to change something and break it all"
The problem is, if we are thinking in changing something it means that's probably (99%) necessary, so we have to.


##  :baby_chick: Which patterns did you use?  Which antipatterns did you remove

| Patterns       | :chicken:           |
| :------:       | :------:            |
| Template       |:white_check_mark:   | 
| Iterator       |:white_check_mark:   |

| Antipatterns            | :chicken:           |
| :------:                | :------:            |
| Spaghetti               |:white_check_mark:   | 
| Lava flow               |:white_check_mark:   | 
| The Blob (god class)    |:white_check_mark:   | 
| Boat anchor             |:white_check_mark:   |
| Fear of success         |:white_check_mark:   |

