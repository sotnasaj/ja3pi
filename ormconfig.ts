module.exports = {
  type: process.env.DB_TYPE || 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: parseInt(process.env.DB_PORT, 10) || 5432,
  username: process.env.DB_USERNAME || 'postgres',
  password: process.env.DB_PASSWORD || 'password',
  database: process.env.DB_DATABASE || 'news_db',
  synchronize: false,
  seeds: ['dist/**/*.seed{.ts,.js}'],
  factories: ['dist/**/*.factory{.ts,.js}'],
  entities: ['dist/**/*.entity{.ts,.js}'],
}